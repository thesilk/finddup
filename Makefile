VERSION=$(shell git describe --abbrev=4 --dirty --always --tags)

LDFLAGS=-ldflags "-w -s -X main.appVersion=${VERSION}"

build:
	go build ${LDFLAGS}

install:
	cp finddup /usr/local/bin/finddup

test:
	go test ./...
