package main

import (
	"flag"
	"fmt"
	"log"
	"os"
)

var (
	appVersion string
	dir        string
)

func initCLI(currentDir string) {
	flag.StringVar(&dir, "dir", currentDir, "directory for checking of duplicates")
	help := flag.Bool("help", false, "help")
	version := flag.Bool("version", false, "version")
	flag.Parse()

	if *help {
		flag.Usage()
		os.Exit(0)
	}

	if *version {
		fmt.Println(appVersion)
		os.Exit(0)
	}
}

func main() {
	currentDir, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	initCLI(currentDir)

	fileHashes := NewFileHash(dir)

	if err := fileHashes.getFiles(); err != nil {
		panic(err)
	}

	if err := fileHashes.calculateHashes(); err != nil {
		panic(err)
	}

	hashDuplicates := getDuplicates(fileHashes.getHashes())
	log.Printf("%+v\n", fileHashes.getHashes())
	log.Printf("%+v\n", hashDuplicates)

	duplicates := newDuplicates(hashDuplicates)

	duplicates.sort()
	duplicates.print()
}
