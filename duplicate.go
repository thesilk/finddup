package main

import (
	"fmt"
	"sort"
)

func getDuplicates(hashMap map[string][]string) map[string][]string {
	hashMapCopy := copyMap(hashMap)
	for key, values := range hashMapCopy {
		if len(values) == 1 {
			delete(hashMapCopy, key)
		}
	}
	return hashMapCopy
}

func copyMap(mapOrig map[string][]string) map[string][]string {
	mapCopy := make(map[string][]string)

	for key, values := range mapOrig {
		mapCopy[key] = values
	}

	return mapCopy
}

type duplicate struct {
	hash     string
	origPath string
	dupPath  string
}

type duplicates struct {
	duplicates []duplicate
}

func newDuplicates(hashMap map[string][]string) *duplicates {
	var d duplicates
	//d.duplicates = []duplicate{}

	for hash, paths := range hashMap {
		d.duplicates = append(d.duplicates, duplicate{
			hash:     hash,
			origPath: paths[0],
			dupPath:  paths[1],
		})
	}
	return &d
}

func (d *duplicate) string() {
	fmt.Printf("| %8s | %50s | %50s |\n", d.hash[:8], d.origPath, d.dupPath)
}

func (d *duplicates) sort() {
	sort.SliceStable(d.duplicates, func(i, j int) bool {
		return d.duplicates[i].origPath < d.duplicates[j].origPath
	})
}

func (d *duplicates) print() {
	for _, d := range d.duplicates {
		d.string()
	}
}
