package main

import (
	"testing"
)

func getTestHashMap() map[string][]string {
	hashMap := make(map[string][]string)
	hashMap["abcdef"] = []string{"a/b", "b/c"}
	hashMap["aaaaaa"] = []string{"a/b"}
	hashMap["bbbbbb"] = []string{"c/d", "d/e", "e/f"}

	return hashMap
}

func getExpectationHashMap() map[string][]string {
	hashMap := make(map[string][]string)
	hashMap["abcdef"] = []string{"a/b", "b/c"}
	hashMap["bbbbbb"] = []string{"d/e", "c/d"}

	return hashMap
}

func getExpectationDuplicates() duplicates {
	return duplicates{duplicates: []duplicate{
		{
			hash:     "abcdef",
			origPath: "a/b",
			dupPath:  "b/c",
		},
		{
			hash:     "bbbbbb",
			origPath: "d/e",
			dupPath:  "c/d",
		},
	},
	}
}

func TestDuplicate(t *testing.T) {
	t.Run("test getDuplicates", testGetDuplicates)
	t.Run("test newDuplicates", testNewDuplicates)
}

func testGetDuplicates(t *testing.T) {
	dup := getDuplicates(getTestHashMap())

	expectation := getExpectationHashMap()
	if len(dup) != len(expectation) {
		t.Errorf("duplicates map doesn't have the same length: expected %d, got %d", len(expectation), len(dup))
	}
	for hash, _ := range dup {
		if _, ok := expectation[hash]; !ok {
			t.Errorf("couldn't find hash %q in expectation %+v", hash, expectation)
		}
	}
}

func testNewDuplicates(t *testing.T) {
	expected := getExpectationDuplicates()

	dupHashMap := getExpectationHashMap()
	actual := newDuplicates(dupHashMap)

	for i, _ := range actual.duplicates {
		failed := true
		for j, _ := range expected.duplicates {
			if actual.duplicates[i].hash == expected.duplicates[j].hash &&
				actual.duplicates[i].origPath == expected.duplicates[j].origPath &&
				actual.duplicates[i].dupPath == expected.duplicates[j].dupPath {
				failed = false
			}
		}
		if failed {
			t.Errorf("failed to create duplicate struct: expected %+v, got %+v", expected, actual)
		}
	}
}
