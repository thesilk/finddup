# finddup
```
 ____  ____  _  _  ____   ____   __  __  ____
( ___)(_  _)( \( )(  _ \ (  _ \ (  )(  )(  _ \
 )__)  _)(_  )  (  )(_) ) )(_) ) )(__)(  )___/
(__)  (____)(_)\_)(____/ (____/ (______)(__)
```

### Description

`finddup` is a command line tool which show all duplicate files for a given directory.


### Install

Supported Platforms:
- Linux
- MacOS

```
$ make && sudo make install
$ which finddup
/usr/local/bin/finddup
```

HINT: Be sure `/usr/local/bin` is in your PATH variable


### Usage

```bash
$ ./finddup --help
Usage of ./finddup:
  -dir string
        directory for checking of duplicates (default "/gitlab/finddup")
  -help
        help
  -version
        version


$ ./finddup --dir /gitlab/finddup
processing hashes: ...  33.3% ...  66.7% ...  100.0%
|85dbb3aa7eda42f1eb245d9f700a10059aadc019725630b9da6b1e552adc927c |/gitlab/finddup/.git/refs/heads/master |/gitlab/finddup/.git/refs/remotes/origin/master |
|e59b47fb71483e85cf7978a208509729b541fe42953ce21effdc0393f8d6f0c9 |/gitlab/finddup/examples1/pictures/pic2.png |/gitlab/finddup/examples1/pictures/pic3.png |
|a948904f2f0f479b8f8197694b30184b0d2ed1c1cd2a1ec0fb85d299a192a447 |/gitlab/finddup/examples1/test1.txt |/gitlab/finddup/examples1/test2.txt |
|35e16f7d1cf660abd09650ab6b933da150575734dd44ae20d380afdcb51a9061 |/gitlab/finddup/examples1/test3.txt |/gitlab/finddup/examples2/test1.txt |
```

### Roadmap

- using GitLab CI for building linux and MacOS binaries
- add unit tests to improve stability
- compare two given directories
- adding interactive mode for deleting (termui or similar)
