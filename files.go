package main

import (
	"crypto/sha256"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

type fileHashCtrl interface {
	getFiles() error
	calculateHashes() error
	getHashes() map[string][]string
}

var _ fileHashCtrl = &files{}

type files struct {
	directory string
	files     []string
	hashes    map[string][]string
}

func NewFileHash(directory string) *files {
	f := files{
		directory: directory,
		hashes:    make(map[string][]string),
	}
	return &f
}

func (f *files) getFiles() error {
	err := filepath.Walk(f.directory, func(path string, fileInfo os.FileInfo, err error) error {
		if !fileInfo.IsDir() {
			f.files = append(f.files, path)
		}
		return nil
	})
	return err
}

func (f *files) calculateHashes() error {
	counter := 1
	fmt.Printf("processing hashes: ")
	for _, file := range f.files {
		if counter%100 == 0 {
			fmt.Printf("... % .1f%% ", float64(counter)/float64(len(f.files))*100)
		}
		hash, err := calculateFileHash(file)
		if err != nil {
			return err
		}

		f.hashes[hash] = append(f.hashes[hash], file)
		counter++
	}
	fmt.Printf("... % .1f%%\n", float64(counter-1)/float64(len(f.files))*100)
	return nil
}

func (f *files) getHashes() map[string][]string {
	return f.hashes
}

func calculateFileHash(file string) (string, error) {
	f, err := os.Open(file)
	if err != nil {
		return "", err
	}
	defer f.Close()

	hash := sha256.New()
	if _, err := io.Copy(hash, f); err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", hash.Sum(nil)), nil
}
